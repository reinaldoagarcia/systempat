﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class FacturasController : Controller
    {
        private FacturacionEntities4 db = new FacturacionEntities4();

        // GET: Facturas
        public ActionResult Index()
        {
            return View(db.Facturas.ToList());
        }

        // GET: Facturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factura factura = db.Facturas.Find(id);
            if (factura == null)
            {
                return HttpNotFound();
            }
            return View(factura);
        }

        // GET: Facturas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Facturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDFactura,IDCliente,Fecha,FormaDePago")] Factura factura)
        {
            if (ModelState.IsValid)
            {
                db.Facturas.Add(factura);
                //db.SaveChanges();
                return RedirectToAction("CreateDetalle");
            }

            return View(factura);
        }

       
        public ActionResult CreateDetalle([Bind(Include = "IDFactura,IDProducto,Descripcion,Precio,Cantidad,Itbis,SubTotal,Total")] DetalleFactura detalle)
        {
            if (ModelState.IsValid)
            {
                detalle.IDFactura = db.Facturas.ToList().Select(f => f.IDFactura).Max();
                //detalle.IDProducto = int.Parse(Request["IDProducto"]);
                //detalle.Itbis = 0;
                //detalle.Precio = decimal.Parse(Request["Precio"]);
                //detalle.Descripcion = Request["Descripcion"];
                //detalle.Total = decimal.Parse(Request["Total"]);

                db.DetalleFacturas.Add(detalle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(detalle);
        }

        // GET: Facturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factura factura = db.Facturas.Find(id);
            if (factura == null)
            {
                return HttpNotFound();
            }
            return View(factura);
        }

        // POST: Facturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDFactura,IDCliente,Fecha,FormaDePago")] Factura factura)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(factura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(factura);
        }

        // GET: Facturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factura factura = db.Facturas.Find(id);
            if (factura == null)
            {
                return HttpNotFound();
            }
            return View(factura);
        }

        // POST: Facturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Factura factura = db.Facturas.Find(id);
            db.Facturas.Remove(factura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
